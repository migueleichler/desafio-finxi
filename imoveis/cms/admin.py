from django.contrib import admin

from .models import Imovel


class ImovelAdmin(admin.ModelAdmin):
    list_display = ('endereco', 'imagem')

admin.site.register(Imovel, ImovelAdmin)
