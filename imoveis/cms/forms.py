from django import forms
from django.core.exceptions import ValidationError

from .models import Imovel, Estado


class CadastroImovelForm(forms.ModelForm):
    endereco = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class' : 'form-control',
                'placeholder' : 'Insira um Endereço',
            }),
        error_messages={
            'invalid': 'Digite o endereço do imóvel.',
            'required' : 'Digite o endereço do imóvel.'
        }
    )
    estado = forms.ModelChoiceField(
        queryset=Estado.objects.all(),
        widget=forms.Select(
            attrs={
                'class' : 'form-control',
            }),
        error_messages={
            'invalid_choice': 'Escolha o Estado de origem do imóvel.',
            'required' : 'Escolha o Estado de origem do imóvel.'
        }
    )
    contato = forms.EmailField(
        widget=forms.TextInput(
            attrs={
                'class' : 'form-control',
                'placeholder' : 'Insira um endereço de Email válido',
            }),
        error_messages={
            'invalid': 'Digite um e-mail em formato válido.',
            'required': 'Digite o e-mail de contato.'
        }
    )
    imagem = forms.ImageField(
        error_messages={
            'invalid': 'Faça o Upload de uma imagem válida. O arquivo enviado ou não é uma imagem ou está corrompido.',
            'invalid_image': 'Faça o Upload de uma imagem válida. O arquivo enviado ou não é uma imagem ou está corrompido.',
            'required': 'Insira a imagem do imóvel.'
        }
    )

    class Meta:
        model = Imovel
        fields = '__all__'

class BuscaForm(forms.ModelForm):
    estado = forms.ModelChoiceField(
        queryset=Estado.objects.all(),
        widget=forms.Select(
            attrs={
                'class' : 'form-control',
            }),
        error_messages={
            'invalid_choice': 'Escolha o Estado de origem do imóvel.',
            'required' : 'Escolha o Estado de origem do imóvel.'
        }
    )
    endereco = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class' : 'form-control',
                'placeholder' : 'Digite Endereço do Imóvel',
            }),
        error_messages={
            'invalid': 'Digite o endereço do imóvel.',
            'required' : 'Digite o endereço do imóvel.'
        }
    )

    class Meta:
        model = Imovel
        fields = ('estado', 'endereco',)
