from django.db import models


class Imovel(models.Model):
    endereco = models.CharField(max_length=200, verbose_name='Endereço do Imóvel')
    estado = models.ForeignKey('Estado', null=True,blank=True, verbose_name='Estado do Imóvel')
    contato = models.CharField(max_length=80, verbose_name='Telefone de Contato')
    imagem = models.ImageField(upload_to = 'img/', default = 'img/None/no-img.jpg')

    class Meta:
	    verbose_name, verbose_name_plural = "Imóvel" , "Imóveis"

    def __str__(self):
	    return "%s" % self.endereco

class Estado(models.Model):
	uf = models.CharField(max_length=2)
	nome = models.CharField(max_length=255)

	class Meta:
		verbose_name, verbose_name_plural = "Estado" , "Estados"
		ordering = ('nome',)

	def __str__(self):
		return "%s" % self.nome
