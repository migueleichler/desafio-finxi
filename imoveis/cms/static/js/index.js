
$(document).ready(function () {
    $('#form-cadastro').validate({
        rules: {
            contato: {
                email: true
            },
            imagem: {
                required: true
            }
        },
        messages: {
            endereco: {
              required: "Digite o endereço do imóvel."
            },
            estado: {
              required: "Escolha o Estado de origem do imóvel."
            },
            contato: {
              required: "Digite o e-mail para contato.",
              email: "Digite um e-mail em formato válido."
            },
            imagem: {
              required: "Insira a imagem do imóvel.",
            }
        }
    });
});
