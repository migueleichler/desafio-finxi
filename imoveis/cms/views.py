from django.shortcuts import render

from .models import Estado, Imovel
from .forms import CadastroImovelForm, BuscaForm

import json

from django.core import serializers
from django.http import HttpResponse
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def index(request):
    form = CadastroImovelForm(request.POST or None, request.FILES or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            form = CadastroImovelForm()
            messages.success(request, 'Imóvel salvo com sucesso.')

    context = {
        'form': form,
    }

    return render(request, 'index.html', context)

def busca(request):
    form = BuscaForm()
    query_imoveis = Imovel.objects.all()
    if request.GET.get('endereco'):
        endereco = request.GET.get('endereco')
        estado = request.GET.get('estado')
        imoveis_proximos = query_imoveis.filter(estado_id=estado).exclude(endereco__iexact=endereco)
        query_imoveis = query_imoveis.filter(endereco__iexact=endereco,
                                             estado_id=estado)
    else:
        imoveis_proximos = []

    imoveis = paginar_queryset(request, query_imoveis)

    return render(request, 'busca.html', { 'form': form,
                                           'imoveis': imoveis,
                                           'imoveis_proximos': imoveis_proximos,
                                           'num_imoveis': query_imoveis.count() })

def get_imoveis(request):
    termo_busca = request.POST.get('search_box')
    data = serializers.serialize('json', Imovel.objects.filter(endereco=termo_busca))

    return HttpResponse(data, content_type='application/json')

def paginar_queryset(request, queryset):
    page = request.GET.get('page', 1)
    paginator = Paginator(queryset, 10)
    try:
        result = paginator.page(page)
    except PageNotAnInteger:
        result = paginator.page(1)
    except EmptyPage:
        result = paginator.page(paginator.num_pages)

    return result
